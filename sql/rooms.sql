CREATE TABLE `rooms` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`lable` varchar(150) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
	`description` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci,
	`is_active` bool,
	`code` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL, 
	PRIMARY KEY (`id`),
	UNIQUE(`lable`)
) 	ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

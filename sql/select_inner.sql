SELECT u.id as user_id, r.id as room_id, u.username, r.lable
FROM rooms as r
JOIN user_to_room as utr
ON r.id = utr.room_id
JOIN users as u ON u.id = utr.user_id;
